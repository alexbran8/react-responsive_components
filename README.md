<!-- OPTIONAL -->
<!-- TODO:develop this file -->
<!-- it should contain a brief app description and the intstructions in order to get it running... -->
<!-- TODO: add project URL (check if this can be dynamic)-->

>În acest repository veți putea regăsi prototipul unui proiect ce în forma sa finală va servi drept componenta din platformă de comunicare între NOKIA și studenți.

>Site-ul web se bazează pe facilitățiile oferite de librăria (JavaScript) React, doridu-se a fi o interfață ușor de utilizat.

<!-- TODO: add some pictures with the app features -->


>Pentru a putea vizualiza și modifica proiectul în Visual Studio Code urmați pașii de mai jos:
>
>1. clone project;\
          **clone link** <https://gitlab.com/alexbran8/react-responsive_components.git>
>2. cd client;
>3. npm start;
>4. http://localhost:3000
