import React from "react";
import Landingpage from "./components/LandingPage/Landingpage.jsx";
import LoginError from "./components/common/LoginError/LoginError";
import Homepage from "./components/Homepage/Homepage";
import Footer from "./components/Footer/Footer"
import {Header} from "./components/Header/Header";
import { HashRouter, Route } from "react-router-dom";
import {UseMemo} from "./components/UseMemo"
import {Credits} from "./components/Credits/Credits"
import {Newsletter} from "./components/Newsletter/Newsletter"
import {FirstArticol} from "./components/Newsletter/FirstArticol/FirstArticol"
import {SecondArticol} from "./components/Newsletter/SecondArticol/SecondArticol"
import {ThirdArticol} from "./components/Newsletter/ThirdArticol/ThirdArticol"


// TODO: 1. check out react Router (HashRouter, BrowserRouter)
//       2. create some possible usefull components for this app
//       3. add components in react router
//       4. configure links from homepage to the above mentioned components

// TODO: (optional) checkout MICROFRONTENDS (MICROSYSTEMS / webpack Federation)


export const AppRouter = (props) => {
  return (
    <HashRouter  {...props} >
      <Route exact path={"/"} component={Landingpage} />
      <Route exact path={"/error"} component={LoginError} />
      <Route exact path={"/home"} component={Homepage} />
      <Route exact path={"/usememo"} component={UseMemo} />
      <Route exact path={"/credits"} component={Credits} />
      <Route exact path={"/newsletter"} component={Newsletter} />
      <Route exact path={"/firstarticol"} component={FirstArticol} />
      <Route exact path={"/secondarticol2"} component={SecondArticol} />
      <Route exact path={"/thirdarticol"} component={ThirdArticol} />
      <Route
        render={({ location }) => {
          if (location.pathname !== "/") {return <><Header /><Footer /></>};
        }}
      />
    </HashRouter>
  );
};
