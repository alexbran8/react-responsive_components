import React from "react";

import "./newsletter.scss"
import { useHistory } from "react-router-dom";

import data from "./jNewsletter.json";

export const Newsletter = () => {

    const history = useHistory();
   

    return (
        <div className="home-page-newsletter">
           {data.map((postData) => {
			console.log(postData);
			return (
           <div className="container " >
		<div className="book" onClick={e => history.push('/firstarticol')}>
			<div className="front">
				<div className="cover">
					<p className="num-up">{postData.auror1}</p>	
                    
                    <p className="author">{postData.titlu1}</p>
				</div>
			</div>
			<div className="left-side">
				<h2>
					<span>{postData.data1}</span>
				</h2>
			</div>
		</div>
	</div>
	);
})}


{data.map((postData) => {
			console.log(postData);
			return (
    <div className="container"  >
		<div className="book" onClick={e => history.push('/secondarticol2')}>
		<div className="front">
				<div className="cover">
					<p className="num-up">{postData.auror2}</p>	
                    
                    <p className="author">{postData.titlu2}</p>
				</div>
			</div>
			<div className="left-side">
				<h2>
					<span>{postData.data2}</span>
				</h2>
			</div>
		</div>
	</div>
);
})}

{data.map((postData) => {
			console.log(postData);
			return (

    <div className="container" >
		<div className="book" onClick={e => history.push('/thirdarticol')}>
		<div className="front">
				<div className="cover">
					<p className="num-up">{postData.auror3}</p>	
                    
                    <p className="author">{postData.titlu3}</p>
				</div>
			</div>
			<div className="left-side">
				<h2>
					<span>{postData.data3}</span>
				</h2>
			</div>
		</div> 
	</div>
  );
})}                  
               
            </div>
       
    )
}

