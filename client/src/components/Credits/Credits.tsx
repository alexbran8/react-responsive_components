import React from "react"

import "./Credits.css"
import { config } from "../../config";
import ReactAudioPlayer from "react-audio-player";

import CardDeck from "react-bootstrap/CardDeck";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import {Component} from "react";
import data from "./jCredist.json";


export const Credits = () => {

    
	const totalItems = 100;

const items = new Array(totalItems).fill(null);
    const url = config.baseURL + "/158066369-space-wars.m4a";

     return( 
        <div className="credits-container" >
             {/* <ReactAudioPlayer
            src={url}
            autoPlay
            loop={false}
            volume={0.3}
          /> */}

            <div className="wrapper">
             
			 
                <div className="scroll-text" >
                
                <Container fluid={true}>
					<Row>
					{items.map((_, idx) =>
						<CardDeck className=" no-gutters " key = {idx}>
							{data.map((postData) => {
								console.log(postData);
								return (
									<div className="credit-persoana">
										<img  src={postData.image} className="imagine"/>
										<div className="tabel">
												<div className="nume">
													Nume: {postData.nume}
												</div>
												<div className="ocupatie">
													Ocupatie:{postData.ocupatie}
												</div>
										</div>
										<br></br>
										<div className="descriere">{postData.descriere}</div>
										<br></br>
									</div>
									);
							})}
						</CardDeck>
)}
						
					</Row>
                    
				</Container>

                </div>

				
            </div>

                </div>



    )
    
}
